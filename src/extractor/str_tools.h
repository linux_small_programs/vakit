#ifndef __STR_TOOLS_H_
#define __STR_TOOLS_H_
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int str_list_size(char **list);
char **str_find(char* _haystack,char* _needle);

#endif // __STR_TOOLS_H_
