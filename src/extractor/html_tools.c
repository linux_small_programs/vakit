#include "html_tools.h"

unsigned int html_source_size(FILE *file){
    fseek(file, 0, SEEK_END); // seek to end of file
    unsigned int size = ftell(file); // get current file pointer
    fseek(file, 0, SEEK_SET); // seek back to beginning of file
	return size;
}

html_source html_source_get(char *file_name){
    int i = 0;
    int character;
    char *buffer = NULL;
    char *buffer_backup = NULL;
    FILE *html_file_for_read;

    html_source result;
    result.buffer_size = 0;
    result.buffer = NULL;

    html_file_for_read= fopen(file_name,"r");
    if ( html_file_for_read== NULL ){
        fprintf(stderr,"File '%s' not found!\n",file_name);
        exit(1);
    }
    unsigned int buffer_size = html_source_size(html_file_for_read);

    buffer = (char*)malloc(buffer_size * sizeof(char));
    if(buffer==NULL){
        fprintf(stderr,"Can't allocate buffer!.\n");
        exit(1);
    }
    do{
        character = fgetc(html_file_for_read);
        buffer[i] = character;
        i++;
        if (i == buffer_size) {
            buffer_size = buffer_size + buffer_size/2;
            buffer_backup = buffer;
            buffer_backup =
                realloc(buffer_backup, buffer_size * sizeof(char));
            if (buffer_backup != NULL) {
                buffer = buffer_backup;
                buffer_backup = NULL;
            }
        }
    }while(character != EOF);

    fclose(html_file_for_read);
    result.buffer_size = strlen(buffer);
    result.buffer = buffer;

    return result;
}

void html_source_del(html_source html){
    if ( html.buffer != NULL )
        free(html.buffer);
}

int is_in_list(char **list,int list_size,char *element){
    for (int i=0;i<list_size;i++){
        if(list[i] == element)
            return 1;
    }
    return 0;
}

int list_size(char **list){
    int i=0;
    while(list[i] != NULL){
        i++;
    }
    return i;
}

int tag_list_size(html_tag* tags){
    int size = 0;
    for(int i=0;tags[i].ht_start != NULL;i++){
        size++;
    }
    return size;
}

void html_s_print_tag(html_tag tag){
    printf("%s\n",tag.ht_start);
}

void html_s_print_tag_wotag(html_tag tag){
    char *handle;
    handle = tag.ht_start + strlen(tag.ht_open_tag);
    while(handle != (tag.ht_end - (strlen(tag.ht_close_tag)+1))){
        printf("%c",*handle);
        handle++;
        }
    printf("\n");
}


void html_f_print_tag(FILE* file,html_tag tag){
    fprintf(file,"%s\n",tag.ht_start);
}

void html_f_print_tag_wotag(FILE* file,html_tag tag){
    char *handle;
    handle = tag.ht_start + strlen(tag.ht_open_tag);
    while(handle != (tag.ht_end - (strlen(tag.ht_close_tag)+1))){
        fprintf(file,"%c",*handle);
        handle++;
        }
}

void html_tag_clear(html_tag *tags){
    int size = tag_list_size(tags);
    for(int i= 0;i<size;i++){
        free(tags[i].ht_start);
    }
    free(tags);
}

html_tag *str_html_find_tags(char *str,char* start_tag,char *end_tag){
    int size = 50;
    void *realloc_holder = NULL;
    html_tag *result = (html_tag*)calloc(size,sizeof(html_tag)*size);
    int tag_piece_size = strlen(start_tag);
    char **html_tag_start = str_find(str,start_tag);
    char **html_tag_end = str_find(str,end_tag);

    int tag_piece = (list_size(html_tag_start) == list_size(html_tag_end))?(list_size(html_tag_start)):0;
    if (tag_piece == 0) {
        printf("tag find error\n");
        free(result);
        exit(1);
        return NULL;
    }
    for(int i = 0;i<tag_piece;i++){
        if(i == size){
            size += size;
            realloc_holder = realloc(result,size);
            if(realloc_holder != NULL ){
                result = (html_tag*)realloc_holder;
                realloc_holder = NULL;
            }
        }
        result[i].ht_start = html_tag_start[i];
        result[i].ht_open_tag = start_tag;
        result[i].ht_close_tag = end_tag;
    }

    for(int i = 0;i<tag_piece;i++){
        char *handle;
        handle = result[i].ht_start;

        int selected_end_tag = i;
        handle = handle+tag_piece_size;
        while(handle != html_tag_end[selected_end_tag]){
            if (is_in_list(html_tag_start,tag_piece,handle)) {
                selected_end_tag++;
            }
            handle++;
        }
        result[i].ht_end = html_tag_end[selected_end_tag];
    }

    for(int i = 0;i<tag_piece;i++){
        result[i].ht_size = (result[i].ht_end - result[i].ht_start) + strlen(result[i].ht_close_tag);
    }

    //Copy tags safe place
    char *holder;
    for (int i= 0; i<tag_piece;i++){
        holder = (char*)calloc(result[i].ht_size,result[i].ht_size+sizeof(char));
        memcpy(holder,result[i].ht_start,result[i].ht_size);
        *(holder + result[i].ht_size) = '\0'; // add null character for string processes
        result[i].ht_start = holder;
        result[i].ht_end = holder + result[i].ht_size+1;
        holder = NULL;
    }

    free(html_tag_start);
    free(html_tag_end);

    return result;
}

html_tag *html_find_tags(html_source html,char *start_tag,char *end_tag){
    unsigned int size = 100;
    html_tag* result = (html_tag*)calloc(size,sizeof(html_tag)*size);
    int tag_piece_size = strlen(start_tag);
    char **html_tag_start = str_find(html.buffer,start_tag);
    char **html_tag_end = str_find(html.buffer,end_tag);

    int tag_piece = (list_size(html_tag_start) == list_size(html_tag_end))?(list_size(html_tag_start)):0;
    if (tag_piece == 0) {
        printf("tag find error\n");
        free(result);
        exit(1);
        return NULL;
    }
    for(int i = 0;i<tag_piece;i++){
        result[i].ht_start = html_tag_start[i];
        result[i].ht_open_tag = start_tag;
        result[i].ht_close_tag = end_tag;
    }

    for(int i = 0;i<tag_piece;i++){
        char *handle;
        handle = result[i].ht_start;

        int selected_end_tag = i;
        handle = handle+tag_piece_size;
        while(handle != html_tag_end[selected_end_tag]){
            if (is_in_list(html_tag_start,tag_piece,handle)) {
                selected_end_tag++;
            }
            handle++;
        }
        result[i].ht_end = html_tag_end[selected_end_tag];
    }

    for(int i = 0;i<tag_piece;i++){
        result[i].ht_size = (result[i].ht_end - result[i].ht_start) + strlen(result[i].ht_close_tag);
    }

    //Copy tags safe place
    char *holder;
    for (int i= 0; i<tag_piece;i++){
        holder = (char*)calloc(result[i].ht_size,result[i].ht_size+sizeof(char));
        memcpy(holder,result[i].ht_start,result[i].ht_size);
        *(holder + result[i].ht_size) = '\0'; // add null character for string processes
        result[i].ht_start = holder;
        result[i].ht_end = holder + result[i].ht_size;
        holder = NULL;
    }

    free(html_tag_start);
    free(html_tag_end);
    return result;
}
