#include "str_tools.h"

int str_list_size(char **list){
    int i=0;
    while(list[i] != NULL){
        i++;
    }
    return i;
}

char **str_find(char* _haystack,char* _needle){
    unsigned int size = 100;
    char **result = (char**)calloc(size,sizeof(char*)*size);
    for(int i=0;i<strlen(_haystack);i++){
        if (i == size) {
            result = realloc(result,sizeof(char*)*(size+100));
            if (result == NULL) {
                printf("Realloc() is failed.\n");
                exit(1);
            }
        }
        if((strlen(_haystack)-i) < strlen(_needle)){
            break;
        }
        for(int j = 0;j<strlen(_needle);j++){
            if(_haystack[i+j] != _needle[j]){
                break;
            }
            else{
                if(j==(strlen(_needle)-1)){
                     result[str_list_size(result)] = (_haystack+i);
                     break;
                }
            }
        }
    }
    return result;
}
