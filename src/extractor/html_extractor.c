#include "html_tools.h"
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc,char** argv){
    char *realloc_holder;
    char *file = "istanbul-icin-namaz-vakti";
    html_source html;
    html = html_source_get(file);
    FILE *extracted = fopen("out.byrm","w");
    if(extracted == NULL){
        printf("Can't open %s file.\n",file);
        exit(1);
    }
    html_tag *tbody = html_find_tags(html,"<tbody>","</tbody>");
    html_source_del(html);
    html_tag *tr = str_html_find_tags(tbody[1].ht_start,"<tr>","</tr>");
    int size = 100;
    html_tag **td = (html_tag **)calloc(size,size*sizeof(html_tag*));
    
    for(int i = 0;tr[i].ht_start != NULL;i++){
        if (i == size){
            size += size;
            realloc_holder = realloc(td,size*sizeof(html_tag*));
            if (realloc_holder != NULL) {
                td = (html_tag**)realloc_holder;
                realloc_holder = NULL;
            }
        }
        td[i] = str_html_find_tags(tr[i].ht_start,"<td>","</td>");
    }
    // print .byrm file
    for(int i = 0; td[i] != NULL;i++){
        for(int j=0;j < tag_list_size(td[i]); j++){
            html_f_print_tag_wotag(extracted,td[i][j]);
            if(j != tag_list_size(td[i]) -1 ){
                fprintf(extracted,",");
            }
        }
        fprintf(extracted,"\n");
    }
    //Clear memory
    for(int i = 0 ; td[i] != NULL;i++){
        free(td[i]);
    }
    free(td);
    html_tag_clear(tr);
    html_tag_clear(tbody);
    fclose(extracted);
    system("rm -f istanbul-icin-namaz-vakti");
	return 0;
}
