#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <stdarg.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <fcntl.h>
#include <string.h> 

#define MAX_LINE 4096



int main(int argc,char *argv[]){
	int status,socket_s,sendbytes,n;
	struct addrinfo hints;
	struct addrinfo *servinfo;
	char sendline[MAX_LINE];
	char recvline[MAX_LINE];

	memset(&hints,0,sizeof(hints));  // empty struct
	hints.ai_family = AF_INET; 	 // IPv4 or IPv6
	hints.ai_socktype = SOCK_STREAM; // tcp stream socket

	if ( (status = getaddrinfo("namazvakitleri.diyanet.gov.tr","80",&hints,&servinfo)) != 0){
		perror("getadrrinfo error.\n");
		exit(1);
	}
	if((socket_s = socket(servinfo->ai_family,servinfo->ai_socktype,servinfo->ai_protocol)) < 0){
		perror("Socket error\n");
		exit(1);
	}

	if((status = connect(socket_s, servinfo-> ai_addr,servinfo->ai_addrlen)) < 0){
		perror("Connection error.");
		exit(1);
	}
	sprintf(sendline,"GET /tr-TR/9541/istanbul-icin-namaz-vakti HTTP/1.1\nHost: namazvakitleri.diyanet.gov.tr\r\n\r\n");
	sendbytes = strlen(sendline);

	if( send(socket_s,sendline,sendbytes,0) != sendbytes){
		perror("Write Error!\n");
		exit(1);
	}

	memset(recvline,0,MAX_LINE);
	FILE *file = fopen("istanbul-icin-namaz-vakti","w");
	char *html = NULL;
	while ((n = recv(socket_s, &recvline, MAX_LINE-1,0)) > 0){
		fprintf(file,"%s",recvline);
		html = strstr(recvline,"</html>");
		if(html != NULL){
			break; //Break when page completed.
		}
		fflush(file);
		memset(recvline,0,MAX_LINE);
		// error checking...
	}
	fprintf(file,"%c",EOF);
	freeaddrinfo(servinfo); //free the link list
	close(socket_s);
	fclose(file);
	return 0;
}
