#ifndef __HTML_TOOLS_H
#define __HTML_TOOLS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "str_tools.h"

typedef struct{
    size_t buffer_size;
    char* buffer;
}html_source;

typedef struct{
    char *ht_open_tag;
    char *ht_close_tag;;
    char *ht_start;
    char *ht_end;
    unsigned int ht_size;
}html_tag;


unsigned int html_source_size(FILE* file);
html_source html_source_get(char *file_name);
void html_source_del(html_source html);
int is_in_list(char**list,int list_size,char *element);
int list_size(char **list);
int tag_list_size(html_tag *tags);
void html_tag_clear(html_tag *tags);
void html_s_print_tag(html_tag tag);
void html_s_print_tag_wotag(html_tag tag);
void html_f_print_tag(FILE *file,html_tag tag);
void html_f_print_tag_wotag(FILE *file,html_tag tag);
html_tag *str_html_find_tags(char *str,char *start_tag,char *end_tag);
html_tag *html_find_tags(html_source html,char* start_tag,char *end_tag);
#endif
