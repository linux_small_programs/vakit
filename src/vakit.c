#include "html_tools.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <limits.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <signal.h>
#include <stdarg.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <fcntl.h>
#include <assert.h>

#define MAX_LINE 4096
#define GMT_TR (+3)

#define MAX_COLUMN 100
struct date
{
    int day;
    int month;
    int hour;
    int minute;
};
char *vakit[] = {"İmsak","Güneş","Öğle","İkindi","Akşam","Yatsı"};
char *ntom(int month);
int mton(char *month);
void generate_files();
struct date take_day_param();
int check_date(char *line,char *current_date);
void calculate_times(char *str,int times[6],unsigned int size);
int int_div(int numerator,int denumerator);
char *find_day_line(char *day_line,struct date date_t,FILE *file,int offset);
void print_remaining_time(int time_name,int times[],int remaining_time);
int program();
int html_download();
int html_extract();

int main(){
    char *dir_file = (char*)malloc(PATH_MAX*sizeof(char));
    char *homedir = getenv("HOME");
    sprintf(dir_file,"%s/.config/vakit/.dir",homedir);
    FILE *file = fopen(dir_file,"r");
    if ( file == NULL){
        fprintf(stderr,".dir file not found!\n");
        exit(1);
    }
    char *cwd = (char*)malloc(PATH_MAX*sizeof(char));
    sprintf(cwd,"%s/.config/vakit",homedir);
    if(cwd == NULL){
        fprintf(stderr,"Cwd find error!\n");
        return -1;
    }   
    int status;
    chdir(cwd);
    status = program();
    if (status == 2)
    {
        generate_files();
        program();
    }
    fclose(file);
    free(cwd);
    free(dir_file);
    return status;
}


char *ntom(int month){    
    char* months[] = {"Ocak","Şubat","Mart","Nisan","Mayıs","Haziran","Temmuz","Ağustos","Eylül","Ekim","Kasım","Aralık"};
    return months[month];
}

int mton(char *month){
   if(strcmp(month,"Ocak") == 0){
       return 0;
   }
   if(strcmp(month,"Şubat") == 0){
       return 1;
   }
   if(strcmp(month,"Mart") == 0){
       return 2;
   }
   if(strcmp(month,"Nisan") == 0){
       return 3;
   }
   if(strcmp(month,"Mayıs") == 0){
       return 4;
   }
   if(strcmp(month,"Haziran") == 0){
       return 5;
   }
   if(strcmp(month,"Temmuz") == 0){
       return 6;
   }
   if(strcmp(month,"Ağustos") == 0){
       return 7;
   }
   if(strcmp(month,"Eylül") == 0){
       return 8;
   }
   if(strcmp(month,"Ekim") == 0){
       return 9;
   }
   if(strcmp(month,"Kasım") == 0){
       return 10;
   }
   if(strcmp(month,"Aralık") == 0){
       return 11;
   }

   return -1;
}

void generate_files(){
    fputs("Generating Files...\n",stdout);
    html_download();
    html_extract();
}

struct date take_day_param(){
    struct date result;
    time_t timer;
    struct tm *timeinfo;
    time(&timer);
    timeinfo = gmtime(&timer);
    

    result.day = timeinfo->tm_mday;
    result.month = timeinfo->tm_mon;
    result.hour = timeinfo->tm_hour + GMT_TR;
    result.minute = timeinfo->tm_min;
    return result;
}

int check_date(char *line,char *current_date){
    int result = 0;
    if (line == NULL){
        return 0;
    }
    unsigned int str_date_len = 0;
    char *str_date = line;
    int space = 0;
    while(*str_date != ',' && space != 2){
       str_date_len++;
       str_date++;
       if(*str_date == ' ')
            space++;
    }
    char *date = calloc(str_date_len,sizeof(char)*(str_date_len+1));
    memcpy(date,line,str_date_len);
    date[str_date_len+1] = '\0';
    if (strcmp(date,current_date) == 0)
        result = 1;

    free(date);
    return result;
}

void calculate_times(char *str,int times[6],unsigned int size){
    char *ptr = (char*)malloc((size)*sizeof(char));
    char *temp = ptr;
    memcpy(ptr,str,size);
    char **str_times = (char **)calloc(6,(sizeof(char*)*6));
    for (int i = 0; i < 6; i++)
    {
        while(*temp != ','){
            temp++;
        }
        *temp = '\0';
        temp++;
        str_times[i] = temp;
    }
    char *holder_hour = (char *)calloc(2,sizeof(char)*3);
    char *holder_min = (char *)calloc(2,sizeof(char)*3);

    for (int i = 0; i < 6; i++)
    {
        for (int j = 0; j < 2; j++)
        {
            holder_hour[j] = str_times[i][j];
            holder_min[j] = str_times[i][j+3];   
        }
        holder_min[3] = '\0';
        holder_hour[3] = '\0';
        times[i] = atoi(holder_hour)*60 + atoi(holder_min);
    }
    free(ptr);
    free(str_times); 
    free(holder_min);
    free(holder_hour);
}

int int_div(int numerator,int denumarator){
    int result;
    int remainder = 0;
    remainder = numerator % denumarator;
    result = (numerator - remainder) / denumarator;
    return result;
}

char *find_day_line(char *day_line,struct date date_t,FILE *file,int offset){
    int status;
    char *current_day = (char*)malloc(sizeof(char)*MAX_COLUMN);
    
    if (date_t.day < 10){
    	sprintf(current_day,"0%d %s",date_t.day + offset,ntom(date_t.month));
    }
    else{
    	sprintf(current_day,"%d %s",date_t.day + offset,ntom(date_t.month));
    }
    
    do{
       day_line = fgets(day_line,MAX_COLUMN,file);
    }while((status = check_date(day_line,current_day)) != 1 && day_line !=NULL);

    if (check_date(day_line,current_day) != 1)
    {
       free(current_day);
       free(day_line);
       return day_line;
    }
 
    free(current_day);
    return day_line;
}

void print_remaining_time(int time_name,int times[],int remaining_time){
    printf("%s vakti zamanı = %d:%d\n",vakit[time_name],int_div(times[time_name],60),times[time_name]%60);
    int remaining_hour = int_div((remaining_time) ,60);
    int remaining_minute = ((remaining_time) % 60) ;

    if(remaining_hour != 0){
        printf("Kalan zaman %d saat %d dakika.\n",remaining_hour,remaining_minute);
    }
    else
    {
        printf("Kalan zaman %d dakika.\n",remaining_minute);
    }
    
}

int program(){
    FILE *file = fopen("out.byrm","r");
    if (file == NULL)
    {
	fputs("File not found...\n",stderr);
        return 2;
    }
    
    char *day_line = (char *)calloc(MAX_COLUMN,sizeof(char)*MAX_COLUMN);
    struct date date = take_day_param();
    day_line = find_day_line(day_line,date,file,0);
    if(day_line == NULL)
    {
	fputs("File out of date...\n",stderr);
        free(day_line);
        fclose(file);
        return 2;
    }
    
    if (day_line == NULL)
    {
        fputs("day_line = Null\n",stderr);
        return -1;
    }
   
    int int_vakitler[6];
    calculate_times(day_line,int_vakitler,strlen(day_line));
    date = take_day_param();
    int secilen_vakit;
    int kalan_zaman;
    int simdiki_zaman = date.hour*60 + date.minute;
    
    for (int i = 0; i < 6; i++)
    {
       if(simdiki_zaman > int_vakitler[5]){
           find_day_line(day_line,date,file,1);
           calculate_times(day_line,int_vakitler,strlen(day_line));
           kalan_zaman = int_vakitler[0] + ((24*60) - simdiki_zaman);
           secilen_vakit = 0;
       } 
       else
       {
           if(simdiki_zaman < int_vakitler[i]){
                secilen_vakit = i;
                kalan_zaman = int_vakitler[secilen_vakit] - (date.hour*60 + date.minute + 1);
                break;
            }
        }
       
    }

    printf("Bugün %d %s\n",date.day,ntom(date.month));
    print_remaining_time(secilen_vakit,int_vakitler,kalan_zaman);    
    if(day_line != NULL){
        free(day_line);
    }
    return 0;
}
int html_download(){
	int status,socket_s,sendbytes,n;
	struct addrinfo hints;
	struct addrinfo *servinfo;
	char sendline[MAX_LINE];
	char recvline[MAX_LINE];

	memset(&hints,0,sizeof(hints));  // empty struct
	hints.ai_family = AF_INET; 	 // IPv4 or IPv6
	hints.ai_socktype = SOCK_STREAM; // tcp stream socket

	if ( (status = getaddrinfo("namazvakitleri.diyanet.gov.tr","80",&hints,&servinfo)) != 0){
		perror("getadrrinfo error.\n");
		exit(1);
	}
	if((socket_s = socket(servinfo->ai_family,servinfo->ai_socktype,servinfo->ai_protocol)) < 0){
		perror("Socket error\n");
		exit(1);
	}

	if((status = connect(socket_s, servinfo-> ai_addr,servinfo->ai_addrlen)) < 0){
		perror("Connection error.");
		exit(1);
	}
	sprintf(sendline,"GET /tr-TR/9541/istanbul-icin-namaz-vakti HTTP/1.1\nHost: namazvakitleri.diyanet.gov.tr\r\n\r\n");
	sendbytes = strlen(sendline);

	if( send(socket_s,sendline,sendbytes,0) != sendbytes){
		perror("Write Error!\n");
		exit(1);
	}

	memset(recvline,0,MAX_LINE);
	FILE *file = fopen("istanbul-icin-namaz-vakti","w");
	char *html = NULL;
	while ((n = recv(socket_s, &recvline, MAX_LINE-1,0)) > 0){
		fprintf(file,"%s",recvline);
		html = strstr(recvline,"</html>");
		if(html != NULL){
			break; //Break when page completed.
		}
		fflush(file);
		memset(recvline,0,MAX_LINE);
		// error checking...
	}
	fprintf(file,"%c",EOF);
	freeaddrinfo(servinfo); //free the link list
	close(socket_s);
	fclose(file);
	return 0;
}

int html_extract(){
    char *realloc_holder;
    char *file = "istanbul-icin-namaz-vakti";
    html_source html;
    html = html_source_get(file);
    FILE *extracted = fopen("out.byrm","w");
    if(extracted == NULL){
        printf("Can't open %s file.\n",file);
        exit(1);
    }
    html_tag *tbody = html_find_tags(html,"<tbody>","</tbody>");
    html_source_del(html);
    html_tag *tr = str_html_find_tags(tbody[1].ht_start,"<tr>","</tr>");
    int size = 100;
    html_tag **td = (html_tag **)calloc(size,size*sizeof(html_tag*));
    
    for(int i = 0;tr[i].ht_start != NULL;i++){
        if (i == size){
            size += size;
            realloc_holder = realloc(td,size*sizeof(html_tag*));
            if (realloc_holder != NULL) {
                td = (html_tag**)realloc_holder;
                realloc_holder = NULL;
            }
        }
        td[i] = str_html_find_tags(tr[i].ht_start,"<td>","</td>");
    }
    // print .byrm file
    for(int i = 0; td[i] != NULL;i++){
        for(int j=0;j < tag_list_size(td[i]); j++){
            html_f_print_tag_wotag(extracted,td[i][j]);
            if(j != tag_list_size(td[i]) -1 ){
                fprintf(extracted,",");
            }
        }
        fprintf(extracted,"\n");
    }
    //Clear memory
    for(int i = 0 ; td[i] != NULL;i++){
        free(td[i]);
    }
    free(td);
    html_tag_clear(tr);
    html_tag_clear(tbody);
    fclose(extracted);
    system("rm -f istanbul-icin-namaz-vakti");
	return 0;
}
