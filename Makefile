conf_dir = $(HOME)/.config/vakit
install: vakit
	mkdir $(conf_dir)
	cp -f vakit $(conf_dir)
	@echo "$(conf_dir)" > $(conf_dir)/.dir
	sudo ln -s $(conf_dir)/$^ /usr/local/bin

reinstall: vakit
	cp -f vakit $(conf_dir)

remove:
	rm -rf $(conf_dir)
	sudo rm -f /usr/local/bin/vakit
